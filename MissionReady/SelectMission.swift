//
//  SelectMission.swift
//  MissionReady
//
//  Created by Daud on 12/26/16.
//  Copyright © 2016 Nex. All rights reserved.
//

import UIKit

class SelectMission: UIViewController, UIScrollViewDelegate {
    
    //View Controllers-------------------------
    var vc : ViewController!
    
    //Constraints-----------------------------
    @IBOutlet weak var contentHeight: NSLayoutConstraint!
    @IBOutlet weak var contentWidth: NSLayoutConstraint!
    
    //UI Elements----------------------------
    @IBOutlet weak var scrView: UIScrollView!
    @IBOutlet weak var missionScr: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    
    var dotViews = [UIView(),UIView(),UIView(),UIView(),UIView()]
    var missionVeiws : [UIView] = []
    var lblTitles = [UILabel(),UILabel(),UILabel(),UILabel(),UILabel()]
    var lblDiscriptions = [UILabel(),UILabel(),UILabel(),UILabel(),UILabel()]
    
    //Variables-------------------------------
    
    // C H A N G E   T H E S E   V A R I A B L E S ----------------
    var titles = ["Cardio","Cardio","Cardio","Cardio","Cardio"]
    var discriptions = ["This is a discription.This is a discription.This is a discription.This is a discription.This is a discription.","This is a discription.This is a discription.This is a discription.This is a discription.This is a discription.","This is a discription.This is a discription.This is a discription.This is a discription.This is a discription.","This is a discription.This is a discription.This is a discription.This is a discription.This is a discription.","This is a discription.This is a discription.This is a discription.This is a discription.This is a discription."]
    var curMission = 0
    //-------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //UI Adjustments------------------------------------
        
        contentWidth.constant = self.view.frame.width
        if (self.view.frame.height <= 568)
        {
            contentHeight.constant = 568
        }
        else
        {
            contentHeight.constant = self.view.frame.height
        }
        self.view.backgroundColor = UIColor.clear
        
        missionScr.delegate = self
        
        view.layoutIfNeeded()
        
        // Adjusting Mission Views----------------------------
        
        missionScr.contentSize.width = missionScr.frame.width * 5
        missionScr.isPagingEnabled = true
        missionScr.clipsToBounds = false
        missionScr.showsHorizontalScrollIndicator = false
        
        for var i in (0 ..< 5)
        {
            
            let vv = UIView(frame: CGRect(x: 5 + (missionScr.frame.width) * CGFloat(i), y: 0, width: missionScr.frame.width - 10, height: missionScr.frame.height))
            vv.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.25)
            vv.layer.cornerRadius = 5
            vv.clipsToBounds = true
            missionScr.addSubview(vv)
            
            let title = UILabel(frame: CGRect(x: 0, y: 0, width: vv.frame.width, height: vv.frame.height * 0.25))
            title.backgroundColor = UIColor(red: 67 / 255, green: 68 / 255, blue: 78 / 255, alpha: 1)
//            title.text = titles[i]
            title.textAlignment = .center
            title.textColor = UIColor.white
            lblTitles[i] = title
            vv.addSubview(title)
            
            let lblDis = UILabel(frame: CGRect(x: 10, y: vv.frame.height * 0.25, width: vv.frame.width - 20, height: vv.frame.height * 0.75))
            lblDis.backgroundColor = UIColor.clear
//            lblDis.text = discriptions[i]
            lblDis.textAlignment = .justified
            lblDis.textColor = UIColor.white
            lblDis.numberOfLines = 0
            lblDiscriptions[i] = lblDis
            vv.addSubview(lblDis)
            
        }
        
        refresh()
        
        //Page Indicating dots---------------------------
        
        let dotContainer = UIView(frame : CGRect(x: 0, y: containerView.frame.height - 5, width: 25 * 5 - 9, height: 5))
        containerView.addSubview(dotContainer)
        
        for var i in (0 ..< 5)
        {
            let dot = UIView(frame: CGRect(x: i * 25, y: 0, width: 7, height: 7))
            dot.backgroundColor = UIColor.white
            dot.layer.cornerRadius = dot.frame.width / 2
            dotViews[i] = dot
            dotContainer.addSubview(dot)
        }
        dotViews[0].backgroundColor = UIColor(red: 255/255, green: 247/255, blue: 138/255, alpha: 1)
        self.dotViews[0].transform = CGAffineTransform(scaleX: 2, y: 2)
        dotContainer.sizeToFit()
        dotContainer.center.x = self.view.frame.width / 2
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func back(_ sender: UIButton) {
        
        vc.back(preView: vc.createProfile.view, curView: self.view)
        
    }
    
    @IBAction func begin(_ sender: UIButton) {
        
        vc.dashboard.view.frame = CGRect(x: self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        (vc.dashboard as! Dashboard).vc = self.vc
        vc.view.addSubview(vc.dashboard.view)
        
        vc.goNext(curView: self.view, nextView: vc.dashboard.view)
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        UIView.animate(withDuration: 0.2, animations: {
            
            for var i in (0 ..< 5)
            {
                self.dotViews[i].backgroundColor = UIColor.white
                self.dotViews[self.curMission].transform = CGAffineTransform(scaleX: 1, y: 1)
                if (self.missionScr.contentOffset.x == self.missionScr.contentSize.width / 5 * CGFloat(i))
                {
                    self.curMission = i
                }
            }
            self.dotViews[self.curMission].backgroundColor = UIColor(red: 255/255, green: 247/255, blue: 138/255, alpha: 1)
            self.dotViews[self.curMission].transform = CGAffineTransform(scaleX: 2, y: 2)
        })
        
    }
    
    //-------------------REFRESH THE VALUES--------------------------
    //Call this method if you want to refresh the values-------------
    func refresh()
    {
        for var i in (0 ..< 5)
        {
            lblTitles[i].text = titles[i]
            lblDiscriptions[i].text = discriptions[i]
        }
    }
    
}

//
//  Dashboard.swift
//  MissionReady
//
//  Created by Daud on 12/29/16.
//  Copyright © 2016 Nex. All rights reserved.
//

import UIKit

class Dashboard: UIViewController,UIScrollViewDelegate {

    //ViewControllers----------------------------
    var vc : ViewController!
    
    //Constraints------------------------------------
    @IBOutlet weak var contentHeight: NSLayoutConstraint!
    @IBOutlet weak var contentWidth: NSLayoutConstraint!
    
    //UI Elements-----------------------------------
    @IBOutlet weak var missionScr: UIScrollView!
    @IBOutlet weak var lblWhistle: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    var dotViews = [UIView(),UIView(),UIView(),UIView(),UIView()]
    
    var lblDetails = [UILabel(),UILabel(),UILabel(),UILabel(),UILabel()]
    var lblOverAlls = [UILabel(),UILabel(),UILabel(),UILabel(),UILabel()]
    var lblThisWeeks = [UILabel(),UILabel(),UILabel(),UILabel(),UILabel()]
    var lblLastScores = [UILabel(),UILabel(),UILabel(),UILabel(),UILabel()]
    
    //Variables-------------------------------
    var headers = ["OVERALL", "THIS WEEK", "LAST SCORE"]
    var curMission = 0
    
    // C H A N G E   T H E S E   V A R I A B L E S ---------------
    var details = ["CARDIO x 10 min","CARDIO x 11 min","CARDIO x 12 min","CARDIO x 13 min","CARDIO x 14 min"]
    var overAlls = ["23%","24%","25%","26%","27%"]
    var thisWeeks = ["0/1","0/2","0/3","0/4","0/5"]
    var lastScores = ["103%","104%","105%","106%","107%"]
    //------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //UI Adjustments------------------------------
        
        contentWidth.constant = self.view.frame.width
        if (self.view.frame.height <= 568)
        {
            contentHeight.constant = 568
        }
        else
        {
            contentHeight.constant = self.view.frame.height
        }

        self.view.backgroundColor = UIColor.clear
        lblWhistle.text = "A few workouts left this week, you’re averaging 103% per workout - Keep it up!"
        lblWhistle.numberOfLines = 0
        
        view.layoutIfNeeded()
        
        // Adjusting Mission Views----------------------------
        
        missionScr.contentSize.width = missionScr.frame.width * 5
        missionScr.isPagingEnabled = true
        missionScr.clipsToBounds = false
        missionScr.showsHorizontalScrollIndicator = false
        missionScr.delegate = self
        
        for var i in (0 ..< 5)
        {
            let vv = UIView(frame: CGRect(x: 5 + (missionScr.frame.width) * CGFloat(i), y: 0, width: missionScr.frame.width - 10, height: missionScr.frame.height))
            vv.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.25)
            vv.layer.cornerRadius = 5
            vv.clipsToBounds = true
            missionScr.addSubview(vv)
            
            vv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.missionTapped(_:))))
            vv.isUserInteractionEnabled = true
            
            let title = UILabel(frame: CGRect(x: 0, y: 0, width: vv.frame.width, height: vv.frame.height * 0.2))
            title.backgroundColor = UIColor(red: 67 / 255, green: 68 / 255, blue: 78 / 255, alpha: 1)
            title.text = "START NEXT WORKOUT"
            title.textAlignment = .center
            title.textColor = UIColor.white
            title.font = UIFont(name: "Oswald-Regular", size: 16)
            vv.addSubview(title)
            
            let topView = UIView(frame: CGRect(x: 0, y: vv.frame.height * 0.2, width: vv.frame.width, height: vv.frame.height * 0.4))
            topView.backgroundColor = UIColor.clear
            
            let bottomView = UIView(frame: CGRect(x: 0, y: vv.frame.height * 0.6, width: vv.frame.width, height: vv.frame.height * 0.4))
            bottomView.backgroundColor = UIColor.clear
            
            let imgView = UIImageView(frame: CGRect(x: 20, y: 20, width: topView.frame.height, height: topView.frame.height - 40))
            imgView.image = UIImage(named: "BootImg.png")
            imgView.contentMode = .scaleAspectFit
            topView.addSubview(imgView)
            
            let lblHeader = UILabel(frame: CGRect(x: imgView.frame.width + 40, y: 15, width: topView.frame.width - imgView.frame.width - 40, height: topView.frame.height / 2 - 15))
            lblHeader.text = "Tabata Intervals"
            lblHeader.textColor = UIColor.white
            lblHeader.font = UIFont(name: "Oswald-Regular", size: 20)
            topView.addSubview(lblHeader)
            
            let lblDetail = UILabel(frame: CGRect(x: imgView.frame.width + 40, y: topView.frame.height / 2, width: topView.frame.width - imgView.frame.width - 40, height: topView.frame.height / 2 - 15))
            lblDetail.text = details[i]
            lblDetails[i] = lblDetail
            lblDetail.textColor = UIColor.white
            lblDetail.font = UIFont(name: "Lato-Regular", size: 14)
            topView.addSubview(lblDetail)
            
            for var j in (0 ..< 3)
            {
                let lblHeader = UILabel(frame: CGRect(x: 0, y: 5, width: bottomView.frame.width / 3, height: bottomView.frame.height / 2 - 15))
                lblHeader.text = headers[j]
                lblHeader.textColor = UIColor.white
                lblHeader.font = UIFont(name: "Lato-Regular", size: 15)
                lblHeader.center.x = bottomView.frame.width / 3 * CGFloat(j) + bottomView.frame.width / 6
                lblHeader.textAlignment = .center
                bottomView.addSubview(lblHeader)
                
                let lblResult = UILabel(frame: CGRect(x: 0, y: lblHeader.frame.origin.y + 25, width: bottomView.frame.width / 3, height: bottomView.frame.height / 2 - 15))
                
                if (j == 0)
                {
                    lblResult.text = overAlls[i]
                    lblOverAlls[i] = lblResult
                }
                else if (j == 1)
                {
                    lblResult.text = thisWeeks[i]
                    lblThisWeeks[i] = lblResult
                }
                else
                {
                    lblResult.text = lastScores[i]
                    lblLastScores[i] = lblResult
                }
                
                lblResult.textColor = UIColor.white
                lblResult.font = UIFont(name: "Oswald-Regular", size: 16)
                lblResult.center.x = bottomView.frame.width / 3 * CGFloat(j) + bottomView.frame.width / 6
                lblResult.textAlignment = .center
                bottomView.addSubview(lblResult)
            }
            
            for var j in (0 ..< 2)
            {
                let seperator = UIView(frame: CGRect(x: 0, y: 0, width: 2, height: bottomView.frame.height * 0.6))
                seperator.center.x = bottomView.frame.width / 3 * (CGFloat(j) + 1)
                seperator.center.y = bottomView.frame.height / 2
                seperator.backgroundColor = UIColor.white
                bottomView.addSubview(seperator)
            }
            
            vv.addSubview(topView)
            vv.addSubview(bottomView)
        }
        
        //Page Indicating dots---------------------------
        
        let dotContainer = UIView(frame : CGRect(x: 0, y: containerView.frame.height - 5, width: 25 * 5 - 9, height: 5))
        containerView.addSubview(dotContainer)
        
        for var i in (0 ..< 5)
        {
            let dot = UIView(frame: CGRect(x: i * 25, y: 0, width: 7, height: 7))
            dot.backgroundColor = UIColor.white
            dot.layer.cornerRadius = dot.frame.width / 2
            dotViews[i] = dot
            dotContainer.addSubview(dot)
        }
        dotViews[0].backgroundColor = UIColor(red: 255/255, green: 247/255, blue: 138/255, alpha: 1)
        self.dotViews[0].transform = CGAffineTransform(scaleX: 2, y: 2)
        dotContainer.sizeToFit()
        dotContainer.center.x = self.view.frame.width / 2
        
        //Profile Image----------------------
        
        imgProfile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.profileTapped(_:))))
        imgProfile.isUserInteractionEnabled = true
        imgProfile.layer.cornerRadius = imgProfile.frame.width / 2
        imgProfile.clipsToBounds = true
        imgProfile.contentMode = .scaleAspectFill
        setProfilePic()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func notification(_ sender: UIButton) {
        
        vc.notifications.view.frame = CGRect(x: self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        (vc.notifications as! Notifications).vc = self.vc
        vc.view.addSubview(vc.notifications.view)
        
        
        vc.goNext(curView: self.view, nextView: vc.notifications.view)
    }
    
    @IBAction func settings(_ sender: UIButton) {
        
        vc.settings.view.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
        (vc.settings as! Settings).vc = self.vc
        vc.view.addSubview(vc.settings.view)
        
        vc.settings.view.frame.origin.y = self.view.frame.height
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.vc.settings.view.frame.origin.y = 20
            self.vc.view.frame.origin.y = -20
            self.view.alpha = 0
            self.vc.imgView.alpha = 0.3
            
        }, completion: {finish in
        })
    }
    
    func profileTapped(_ : UITapGestureRecognizer)
    {
        vc.editProfile.view.frame = CGRect(x: self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        (vc.editProfile as! EditProfile).vc = self.vc
        vc.view.addSubview(vc.editProfile.view)
        
        vc.goNext(curView: self.view, nextView: vc.editProfile.view)
        vc.curView = "editProfile"
    }
    
    func missionTapped(_ : UITapGestureRecognizer)
    {
        vc.workoutOverview.view.frame = CGRect(x: self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        (vc.workoutOverview as! WorkoutOverview).vc = self.vc
        vc.view.addSubview(vc.workoutOverview.view)
        
        vc.goNext(curView: self.view, nextView: vc.workoutOverview.view)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        UIView.animate(withDuration: 0.2, animations: {
            
            for var i in (0 ..< 5)
            {
                self.dotViews[i].backgroundColor = UIColor.white
                self.dotViews[self.curMission].transform = CGAffineTransform(scaleX: 1, y: 1)
                if (self.missionScr.contentOffset.x == self.missionScr.contentSize.width / 5 * CGFloat(i))
                {
                    self.curMission = i
                }
            }
            self.dotViews[self.curMission].backgroundColor = UIColor(red: 255/255, green: 247/255, blue: 138/255, alpha: 1)
            self.dotViews[self.curMission].transform = CGAffineTransform(scaleX: 2, y: 2)
        })

    }
    
    //SET PROFILE PIC-----------------------------------------------
    func setProfilePic()
    {
        let userDefault = UserDefaults.standard
        if (userDefault.data(forKey: "profilePic") == nil)
        {
            imgProfile.image = UIImage(named: "Avater.png")
        }
        else
        {
            imgProfile.image = UIImage(data: userDefault.data(forKey: "profilePic")!)
        }
    }
    
    //-------------------REFRESH THE VALUES--------------------------
    //Call this method if you want to refresh the values-------------
    func refresh()
    {
        for var i in (0 ..< 5)
        {
            lblDetails[i].text = details[i]
            lblOverAlls[i].text = overAlls[i]
            lblThisWeeks[i].text = thisWeeks[i]
            lblLastScores[i].text = lastScores[i]
        }
    }

}

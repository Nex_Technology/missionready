//
//  Settings.swift
//  MissionReady
//
//  Created by Daud on 1/2/17.
//  Copyright © 2017 Nex. All rights reserved.
//

import UIKit

class Settings: UIViewController {

    var vc : ViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.view.frame.origin.y = self.view.frame.height
            self.vc.view.frame.origin.y = 0
            self.vc.dashboard.view.alpha = 1
            self.vc.imgView.alpha = 1
            
        }, completion: {finish in
            self.removeFromParentViewController()
        })
        
    }
    
}

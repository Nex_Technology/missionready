//
//  WorkoutOverview.swift
//  MissionReady
//
//  Created by Daud on 1/3/17.
//  Copyright © 2017 Nex. All rights reserved.
//

import UIKit

class WorkoutOverview: UIViewController, UIScrollViewDelegate {

    //View Containers--------------------------------------
    var vc : ViewController!
    
    //Constraints------------------------------------------
    @IBOutlet weak var contentHeight: NSLayoutConstraint!
    @IBOutlet weak var contentWidth: NSLayoutConstraint!
    @IBOutlet weak var imgTop: NSLayoutConstraint!
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    @IBOutlet weak var btnBackTop: NSLayoutConstraint!
    
    //UIElements-------------------------------------------
    @IBOutlet weak var scrView: UIScrollView!
    
    var views : [UIView] = []
    
    //Variables------------------------------------------
    
    // C H A N G E   T H E S E   V A R I A B L E S ----------------
    var headers = ["Header1","Header2","Header3","Header4","Header5","Header6","Header7","Header8"]
    var details = ["Detail1","Detail2","Detail3","Detail4","Detail5","Detail6","Detail7","Detail8"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //UI Adjustments------------------------------------
        
        contentWidth.constant = self.view.frame.width
        if (self.view.frame.height <= 1068)
        {
            contentHeight.constant = 1068
        }
        else
        {
            contentHeight.constant = self.view.frame.height
        }
        self.view.backgroundColor = UIColor.clear
        
        scrView.delegate = self
        
        refresh()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(_ sender: UIButton) {
        vc.back(preView: vc.dashboard.view, curView: self.view)
    }
    
    @IBAction func begin(_ sender: UIButton) {
        
        vc.workoutScreen.view.frame = CGRect(x: self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        (vc.workoutScreen as! WorkoutScreen).vc = self.vc
        vc.view.addSubview(vc.workoutScreen.view)
        
        vc.goNext(curView: self.view, nextView: vc.workoutScreen.view)
        
    }
    
    
    //Scroll View Delegate Methods--------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
//        self.endEditing(true)
        let scrOffset = scrView.contentOffset.y
        if (scrOffset < 0)
        {
            imgTop.constant = scrOffset
            imgHeight.constant = 240 + scrOffset * -1
            btnBackTop.constant = 32 + scrOffset
        }
        
    }
    
    //-------------------REFRESH THE VALUES--------------------------
    //Call this method if you want to refresh the values-------------
    func refresh()
    {
        
        view.layoutIfNeeded()
        
        for vv in views
        {
            vv.removeFromSuperview()
        }
        views.removeAll()
        
        for var i in (0 ..< headers.count)
        {
            let vv = UIView(frame: CGRect(x: 16, y: 350 + CGFloat(i) * 100, width: self.view.frame.width - 32, height: 85))
            vv.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.4)
            vv.layer.cornerRadius = 3
            vv.clipsToBounds = true
            views.append(vv)
            self.scrView.addSubview(vv)
            
            let title = UILabel(frame: CGRect(x: 0, y: 0, width: vv.frame.width, height: vv.frame.height * 0.5))
            title.backgroundColor = UIColor(red: 67 / 255, green: 68 / 255, blue: 78 / 255, alpha: 1)
            title.text = headers[i]
            title.font = UIFont(name: "Oswald-Regular", size: 18)
            title.textAlignment = .center
            title.textColor = UIColor.white
            vv.addSubview(title)
            
            let lblDis = UILabel(frame: CGRect(x: 10, y: vv.frame.height * 0.5, width: vv.frame.width - 20, height: vv.frame.height * 0.5))
            lblDis.backgroundColor = UIColor.clear
            lblDis.text = details[i]
            lblDis.font = UIFont(name: "Lato-Regular", size: 18)
            lblDis.textAlignment = .justified
            lblDis.textColor = UIColor.white
            lblDis.numberOfLines = 0
            vv.addSubview(lblDis)
            
            contentHeight.constant = vv.frame.origin.y + 220
        }
    }

}

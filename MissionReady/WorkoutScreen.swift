//
//  WorkoutScreen.swift
//  MissionReady
//
//  Created by Daud on 1/4/17.
//  Copyright © 2017 Nex. All rights reserved.
//

import UIKit

class WorkoutScreen: UIViewController, UIScrollViewDelegate {

    //ViewControllers-------------------
    var vc : ViewController!
    
    //UI Element------------------------
    @IBOutlet weak var scrView: UIScrollView!
    @IBOutlet weak var lblCurMission: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    let btnTime = [UIButton(),UIButton(),UIButton(),UIButton(),UIButton(),UIButton(),UIButton()]
    let btnHeartRate = [UIButton(),UIButton(),UIButton(),UIButton(),UIButton(),UIButton(),UIButton()]
    
    let blackView = UIView()
    var dotViews = [UIView(),UIView(),UIView(),UIView(),UIView(),UIView(),UIView()]
    
    //Variables-------------------
    var titles = ["","",""]
    var curHeartRates = [80,70,60,50,40,50,40]
    var curPopUp = 0
    var curMission = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clear
        
        blackView.backgroundColor = UIColor.black
        blackView.frame = self.view.frame
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissPopUp(_:))))
        
        // Adjusting Mission Views----------------------------
        
        scrView.contentSize.width = scrView.frame.width * 7
        scrView.isPagingEnabled = true
        scrView.clipsToBounds = false
        scrView.showsHorizontalScrollIndicator = false
        scrView.delegate = self
        
        for var i in (0 ..< 7)
        {
            
            let vv = UIView(frame: CGRect(x: 5 + (scrView.frame.width) * CGFloat(i), y: 0, width: scrView.frame.width - 10, height: scrView.frame.height))
            vv.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.25)
            vv.layer.cornerRadius = 5
            vv.clipsToBounds = true
            scrView.addSubview(vv)
            
            let blackView = UIView(frame: CGRect(x: 0, y: 0, width: vv.frame.width, height: vv.frame.height * 0.4))
            blackView.backgroundColor = UIColor(red: 67 / 255, green: 68 / 255, blue: 78 / 255, alpha: 1)
            vv.addSubview(blackView)
            
            let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 30))
            imgView.image = UIImage(named: "Whistle Icon.png")
            imgView.contentMode = .scaleAspectFit
            imgView.center.y = blackView.frame.height / 2
            blackView.addSubview(imgView)
            
            let title = UILabel(frame: CGRect(x: 70, y: 0, width: blackView.frame.width - 80, height: blackView.frame.height))
            title.text = "You’re Doing Great! Keep it up! Just  a few more to go."
            title.font = UIFont(name: "Lato-Regular", size: 16)
            title.numberOfLines = 0
            title.textColor = UIColor.white
            blackView.addSubview(title)
            
            var lbls = ["TIME", "HR PPM"]
            
            for var i in (0 ..< 2)
            {
                var lblHeader = UILabel(frame : CGRect(x: vv.frame.width / 2 * CGFloat(i), y: (vv.frame.height * 0.4) + 25, width: vv.frame.width / 2, height: 15))
                lblHeader.text = lbls[i]
                lblHeader.font = UIFont(name: "Oswald-Regular", size: 12)
                lblHeader.textColor = UIColor.white
                lblHeader.textAlignment = .center
                vv.addSubview(lblHeader)
            }
            
            btnTime[i].frame = CGRect(x: 0, y: (vv.frame.height * 0.4) + 60, width: vv.frame.width / 2, height: 40)
            btnTime[i].setTitle("3m20s", for: .normal)
            btnTime[i].titleLabel?.font = UIFont(name: "Oswald-Regular", size: 35)
            btnTime[i].setTitleColor(UIColor.white, for: .normal)
            btnTime[i].addTarget(self, action: #selector(self.adjustTime(_:)), for: .touchUpInside)
            vv.addSubview(btnTime[i])
            
            btnHeartRate[i].frame = CGRect(x: vv.frame.width / 2, y: (vv.frame.height * 0.4) + 60, width: vv.frame.width / 2, height: 40)
            btnHeartRate[i].setTitle("\(curHeartRates[i])s", for: .normal)
            btnHeartRate[i].titleLabel?.font = UIFont(name: "Oswald-Regular", size: 35)
            btnHeartRate[i].setTitleColor(UIColor(red: 255 / 255, green: 247 / 255, blue: 138 / 255, alpha: 1), for: .normal)
            btnHeartRate[i].tag = Int(i)
            btnHeartRate[i].addTarget(self, action: #selector(self.adjustInput(_:)), for: .touchUpInside)
            vv.addSubview(btnHeartRate[i])
            
        }
        
        //Page Indicating dots---------------------------
        
        let dotContainer = UIView(frame : CGRect(x: 0, y: lblCurMission.frame.origin.y + 30, width: 25 * 7 - 12, height: 5))
        containerView.addSubview(dotContainer)
        
        for var i in (0 ..< 7)
        {
            let dot = UIView(frame: CGRect(x: i * 25, y: 0, width: 12, height: 7))
            dot.backgroundColor = UIColor.white
            dot.layer.cornerRadius = dot.frame.height / 2
            dotViews[i] = dot
            dotContainer.addSubview(dot)
        }
        dotViews[0].backgroundColor = UIColor(red: 255/255, green: 247/255, blue: 138/255, alpha: 1)
        dotContainer.center.x = containerView.frame.width / 2
        
    }
    
    func adjustInput(_ btn : UIButton)
    {
        vc.adjustInput.view.frame = CGRect(x: 0, y: self.view.frame.height, width: 250, height: 180)
        (vc.adjustInput as! AdjustInput).vc = self.vc
        vc.adjustInput.view.center.x = self.view.frame.width / 2
        vc.view.addSubview(vc.adjustInput.view)
        (vc.adjustInput as! AdjustInput).setTime(rate: curHeartRates[btn.tag], curIndex : btn.tag)
        
        blackView.alpha = 0
        self.view.addSubview(blackView)
        curPopUp = 0
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.vc.adjustInput.view.center.y = self.view.frame.height / 2
            self.blackView.alpha = 0.6
            
        }, completion: {finish in
        })
    }
    
    func adjustTime(_: UIButton)
    {
        vc.adjustTime.view.frame = CGRect(x: 0, y: self.view.frame.height, width: 250, height: 180)
        (vc.adjustTime as! AdjustTime).vc = self.vc
        vc.adjustTime.view.center.x = self.view.frame.width / 2
        vc.view.addSubview(vc.adjustTime.view)
        
        blackView.alpha = 0
        self.view.addSubview(blackView)
        curPopUp = 1
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.vc.adjustTime.view.center.y = self.view.frame.height / 2
            self.blackView.alpha = 0.6
            
        }, completion: {finish in
        })
    }
    
    func dismissPopUp(_: UITapGestureRecognizer)
    {
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
        
        if (self.curPopUp == 0)
        {
            self.vc.adjustInput.view.center.y = self.view.frame.height / 2 + self.view.frame.height
        }
        else if (self.curPopUp == 1)
        {
            self.vc.adjustTime.view.center.y = self.view.frame.height / 2 + self.view.frame.height
        }
        else
        {
            self.vc.rest.view.center.y = self.view.frame.height / 2 + self.view.frame.height
        }
        self.blackView.alpha = 0
        
    }, completion: {finish in
        self.blackView.removeFromSuperview()
        if (self.curPopUp == 0)
        {
            self.vc.adjustInput.view.removeFromSuperview()
        }
        else if (self.curPopUp == 1)
        {
            self.vc.adjustTime.view.removeFromSuperview()
        }
        else if (self.curPopUp == 2)
        {
            self.vc.rest.view.removeFromSuperview()
        }
        else
        {
            self.vc.rest.view.removeFromSuperview()
            self.goNext()
        }
    })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(_ sender: Any) {
        vc.back(preView: vc.workoutOverview.view, curView: self.view)
    }
    
    @IBAction func begin(_ sender: Any) {
        
        vc.rest.view.frame = CGRect(x: 0, y: self.view.frame.height, width: 250, height: 180)
        (vc.rest as! Rest).vc = self.vc
        vc.rest.view.center.x = self.view.frame.width / 2
        vc.view.addSubview(vc.rest.view)
        
        blackView.alpha = 0
        self.view.addSubview(blackView)
        curPopUp = 2
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.vc.rest.view.center.y = self.view.frame.height / 2
            self.blackView.alpha = 0.6
            
        }, completion: {finish in
        })
    }
    
    func goNext()
    {
        vc.workOutResults.view.frame = CGRect(x: self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        (vc.workOutResults as! WorkOutResults).vc = self.vc
        vc.view.addSubview(vc.workOutResults.view)
        
        vc.goNext(curView: self.view, nextView: vc.workOutResults.view)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for var i in (0 ..< 7)
        {
            if (scrView.contentOffset.x == scrView.contentSize.width / 7 * CGFloat(i))
            {
                curMission = i
            }
        }
        lblCurMission.text = "SET \(curMission + 1)/7"
        
        
        
        UIView.animate(withDuration: 0.2, animations: {
            
            for var i in (0 ..< 7)
            {
                if (self.scrView.contentOffset.x == self.scrView.contentSize.width / 5 * CGFloat(i))
                {
                    self.curMission = i
                }
            }
            
            for var i in (0 ..< self.curMission + 1)
            {
                self.dotViews[i].backgroundColor = UIColor(red: 255/255, green: 247/255, blue: 138/255, alpha: 1)
            }
            for var i in (self.curMission + 1 ..< 7)
            {
                self.dotViews[i].backgroundColor = UIColor.white
            }
        })
        
    }
    
    
    //REFRESH HEART RATE---------------------------------------------
    func refreshHeartRate()
    {
        for var i in (0 ..< 7)
        {
            btnHeartRate[i].setTitle("\(curHeartRates[i])s", for: .normal)
        }
    }
    
    
    
}

//
//  SignIn.swift
//  MissionReady
//
//  Created by Daud on 12/26/16.
//  Copyright © 2016 Nex. All rights reserved.
//

import UIKit

class SignIn: UIViewController {

    //View Controllers------------------
    var vc : ViewController!
    
    @IBOutlet weak var header_offsetY: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //UI Adjustments--------------------------
        
        if (self.view.frame.height <= 568)
        {
            header_offsetY.constant = 40
        }
        
        self.view.backgroundColor = UIColor.clear
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signIn_FB(_ sender: UIButton) {
    }
    
    @IBAction func signIn_Insta(_ sender: UIButton) {
    }
    
    // Sign Up with Email button is pressed----------------------------------------------------------------
    
    @IBAction func signIn_Email(_ sender: UIButton) {
        
        vc.signInEmail.view.frame = CGRect(x: self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        (vc.signInEmail as! SignInEmail).vc = self.vc
        vc.view.addSubview(vc.signInEmail.view)
        
        vc.goNext(curView: self.view, nextView: vc.signInEmail.view)
        vc.curView = "signInEmail"
        
    }

}

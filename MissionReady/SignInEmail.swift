//
//  SignInEmail.swift
//  MissionReady
//
//  Created by Daud on 12/27/16.
//  Copyright © 2016 Nex. All rights reserved.
//

import UIKit

class SignInEmail: UIViewController, UIScrollViewDelegate {

    // View Controllers------------------------
    var vc : ViewController!
    
    //Elements-------------------------
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtRePassword: UITextField!
    @IBOutlet weak var lblTerms: UILabel!
    @IBOutlet weak var btnSignUp: UIButton!
    
    //Constraints-------------------------
    
    @IBOutlet weak var lblHeader_Top: NSLayoutConstraint!
    @IBOutlet weak var btnBack_Top: NSLayoutConstraint!
    
    //Variables---------------------------
    var keyboardShowed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // UI Adjustments-------------------------------------------------
        
        lblTerms.text = "By pressing \"Sign UP\", you agree to MissionReady's Terms and Services and Privacy Policy"
        lblTerms.numberOfLines = 0
        self.view.backgroundColor = UIColor.clear
        
        //Tap to hide keyboard
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.keyboardHide(_:))))
        self.view.isUserInteractionEnabled = true
        
        //Keyboard notification to listen to keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //to get the keyboard height--------------------
    func keyboardWillShow(notification: NSNotification)
    {
        if (!keyboardShowed && vc.curView == "signInEmail")
        {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue{
                //            let keyboardHeight = keyboardSize.height
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: {
                    
                    self.lblHeader_Top.constant = self.lblHeader_Top.constant - (self.txtEmail.frame.origin.y - 20) + 50
                    self.btnBack_Top.constant = self.btnBack_Top.constant - (self.txtEmail.frame.origin.y - 20) + 50
                    
                    //                self.view.backgroundColor = UIColor(red: 110 / 255, green: 136 / 255, blue: 102 / 255, alpha: 0.3)
                    self.view.frame.origin.y = -(self.txtEmail.frame.origin.y - 20) + 50
                    self.view.layoutIfNeeded()
                    
                }, completion: nil)
                keyboardShowed = true
                
            }
        }
    }
    
    //Hide Keyboard----------------------------
    func keyboardHide(_ gesture : UITapGestureRecognizer)
    {
        if (keyboardShowed)
        {
            self.view.endEditing(true)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: {
                
                self.lblHeader_Top.constant = self.lblHeader_Top.constant + (self.txtEmail.frame.origin.y - 20) - 50
                self.btnBack_Top.constant = self.btnBack_Top.constant + (self.txtEmail.frame.origin.y - 20) - 50
                
                //            self.view.backgroundColor = UIColor(red: 110 / 255, green: 136 / 255, blue: 102 / 255, alpha: 0.3)
                self.view.frame.origin.y = 0
                self.view.layoutIfNeeded()
                
            }, completion: nil)
            keyboardShowed = false
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        
        vc.back(preView: vc.signIn.view, curView: self.view)
        keyboardHide(UITapGestureRecognizer())
    }
    
    @IBAction func signUp(_ sender: UIButton) {
        
        vc.createProfile.view.frame = CGRect(x: self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        (vc.createProfile as! CreateProfile).vc = self.vc
        vc.view.addSubview(vc.createProfile.view)
        
        vc.goNext(curView: self.view, nextView: vc.createProfile.view)
        vc.curView = "createProfile"
        keyboardHide(UITapGestureRecognizer())
        
    }

}
